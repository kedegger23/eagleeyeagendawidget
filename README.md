# README Eagle Eye Agenda Widget#

EagleEyeAgendaWidget is a simple widget for your home screen showing the agenda for the next two weeks taken from your Google calendars. By using a tiny (but still readable) font, the screen estate.

## How-To install ##

1. Download the [latest version](http://bit.ly/2993mwo) (bit.ly is just used for tracking the number of downloads)
2. Install via adb or by selecting the APK on your phone (Make sure, to allow the install from unknown sources "Settings/Security/Unknown sources" and revert it back after installing for security reasons)
3. Add the widget to your home screen

### What is this repository for? ###

* Current Version 1.8

### Contribution guidelines ###

* Be respectful 
* Share and distribute freely - give credit 
* Feel free to [submit an issue](https://bitbucket.org/kedegger23/eagleeyeagendawidget/issues/new) if you find a bug or have an idea for improvement 

### Who do I talk to? ###

Feel free to drop me a message if you want to share...