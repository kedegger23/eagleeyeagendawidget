package com.edegger.calendar.widget;

import com.edegger.calendar.CalendarChangeReceiver;
import com.edegger.calendar.R;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.widget.RemoteViews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EagleEyeAgendaWidgetProvider extends AppWidgetProvider {

    private static final Logger LOG = LoggerFactory.getLogger(EagleEyeAgendaWidgetProvider.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        LOG.debug("Provider.onReceive: "+intent.toUri(Intent.URI_INTENT_SCHEME));
        super.onReceive(context, intent);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        LOG.debug( "Provider.onDelete ");
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        LOG.debug("Provider.onEnabled");

    }

    @Override
    public void onUpdate(Context context, android.appwidget.AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {
        LOG.info("Will update {} widgets", appWidgetIds.length);
        for (int appWidgetId : appWidgetIds) {
            final Intent intent = new Intent(context,EagleEyeAgendaWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
            PendingIntent launchCalendar = getCalendarLaunchIntent(context, appWidgetId);
            final RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.calendar_widget);
            rv.setRemoteAdapter(R.id.evt_list, intent);
            rv.setScrollPosition(R.id.evt_list, 0);
            rv.setEmptyView(R.id.evt_list, R.id.empty_view);
            rv.setOnClickPendingIntent(R.id.calendar_widget_frame,launchCalendar);
            rv.setPendingIntentTemplate(R.id.evt_list, launchCalendar);
            LOG.debug("Updating remote views for widget id {}",appWidgetId);
            appWidgetManager.updateAppWidget(appWidgetId, rv);
            //tell the widget manager, that the content has changed
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,R.id.evt_list);
        }
        LOG.debug( "All widgets updated");
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private PendingIntent getCalendarLaunchIntent(final Context ctx, int widgetId) {
        Intent app_launch;
        if (EagleEyeAgendaWidgetConfigure.launchGoogleCalendar(ctx, widgetId)) {
            LOG.debug("Will launch the internal calendar app on click");
            //this will _always_ launch the internal calendar app
            long startMillis = System.currentTimeMillis();
            Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
            builder.appendPath("time");
            ContentUris.appendId(builder, startMillis);
            app_launch = new Intent(Intent.ACTION_VIEW).setData(builder.build());
        } else {
            LOG.debug("Will launch any calendar app on click");
            app_launch = new Intent();
            // mimeType will popup the chooser any for any implementing
            // application (e.g. the built in calendar or applications such as
            // "Business calendar"
            //this won't work for the internal calendar app -> only flashes and
            //quits right after
            app_launch.setType("vnd.android.cursor.item/event");
            app_launch.setAction(Intent.ACTION_VIEW);

        }
        return PendingIntent.getActivity(ctx, 0,app_launch, PendingIntent.FLAG_UPDATE_CURRENT);

    }

}