package com.edegger.calendar.widget;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.provider.CalendarContract;
import android.text.format.DateUtils;

import com.edegger.calendar.R;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by EdeggerK on 12.02.2018.   ¯\_(ツ)_/¯
 */

public class UpdateCalendarWidgetJob extends JobService {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateCalendarWidgetJob.class);
    private static final int JOB_ID = 832974645;
    static final Uri CALENDAR_URI = Uri.parse("content://" + CalendarContract.AUTHORITY + "/");


    @Override
    public boolean onStartJob(JobParameters params) {
        LOG.info("onStartJob");
        notifyWidgets(this);
        //we are stopped right away -> not long running
        jobFinished(params, false);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        LOG.info("onStopJob");
        //no need for reschedule
        return false;
    }

    private void notifyWidgets(Context ctx) {
        LOG.debug("Will notify widgets about new content");
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(ctx);
        ComponentName widgetComponent = new ComponentName(ctx,EagleEyeAgendaWidgetProvider.class);
        int[] widgetIds = appWidgetManager.getAppWidgetIds(widgetComponent);
        appWidgetManager.notifyAppWidgetViewDataChanged(widgetIds, R.id.evt_list);

    }



    public static void scheduleContentObserverJob(Context ctx) {
        LOG.info("Scheduling a content observer job to update the widget");
        JobScheduler scheduler = (JobScheduler) ctx.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID,
                new ComponentName( ctx.getPackageName(), UpdateCalendarWidgetJob.class.getName()));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NONE)
                .setPersisted(false) //we must no persist the job, when it's triggered by content
                .addTriggerContentUri(new JobInfo.TriggerContentUri(
                        CALENDAR_URI,
                        JobInfo.TriggerContentUri.FLAG_NOTIFY_FOR_DESCENDANTS))
                .setTriggerContentMaxDelay(DateUtils.MINUTE_IN_MILLIS*1)
                .setOverrideDeadline(DateUtils.MINUTE_IN_MILLIS);
        int scheduled = scheduler.schedule(builder.build());
        LOG.debug("Content observer is scheduled: {}",JobScheduler.RESULT_SUCCESS==scheduled?"success":"failed");
    }

}
