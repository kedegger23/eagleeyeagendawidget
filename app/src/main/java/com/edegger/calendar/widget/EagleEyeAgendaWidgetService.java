package com.edegger.calendar.widget;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.edegger.calendar.R;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EagleEyeAgendaWidgetService extends RemoteViewsService {

    private static final Logger LOG = LoggerFactory.getLogger(EagleEyeAgendaWidgetService.class);

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        LOG.debug("onGetViewFactory: "+intent.toUri(Intent.URI_INTENT_SCHEME));
        return new CalendarRemoteViewsFactory(this.getApplicationContext(),intent);
    }

}

class CalendarRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private static final Logger LOG = LoggerFactory.getLogger(CalendarRemoteViewsFactory.class);

    private final static int COUNT_VIEWS = 14; // excluding the invisible ones :)
    private final Context mContext;
    private final int mWidgetId;
    private RemoteViews[] rvs = new RemoteViews[COUNT_VIEWS];
    private static int MAX_SKIPPED_DAYS = 100;

    private static final String[] COLS = new String[] {
            CalendarContract.Events.TITLE, CalendarContract.Instances.BEGIN,
            CalendarContract.Instances.END, CalendarContract.Events.ALL_DAY,
            CalendarContract.Events.CALENDAR_COLOR };

    public CalendarRemoteViewsFactory(Context applicationContext, Intent intent) {
        LOG.debug("creating factory");
        mContext = applicationContext;
        mWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        LOG.debug( "factory is serving widget: " + mWidgetId);
        // mActiveCalendarIds = CalendarWidgetConfigure.listActiveCalendarIds(
        // applicationContext, mWidgetId);
    }

    @Override
    public int getCount() {
        // LOG.debug( "getCount");
        return rvs.length;
    }

    @Override
    public long getItemId(int position) {
        LOG.debug( "getItemId " + position);
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        // LOG.debug( "getLoadingView");
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        LOG.debug( "getView@" + position);
        return rvs[position];
    }


    @Override
    public int getViewTypeCount() {
        // LOG.debug( "getViewTypeCount");
        // if i return 1 here, there is an exception killing the launcher :(
        return 2;
    }


    @Override
    public boolean hasStableIds() {
        // LOG.debug( "hasStableIds");
        return false;
    }

    @Override
    public void onCreate() {
        LOG.debug( "onCreate");
        // Since we reload the cursor in onDataSetChanged() which gets called
        // immediately after
        // onCreate(), we do nothing here.
    }


    @Override
    public void onDataSetChanged() {
        //this will only be called if
        // appWidgetManager.notifyAppWidgetViewDataChanged(widgetIds, R.id.evt_list);
        // is called (in CalendarChangeReceiver
        LOG.debug("onDataSetChanged");
        updateRvs();
        //update the widget
    }

    private RemoteViews getDayEventView(final Time date, final List<Event> dateEvts) {
        LOG.trace( "returning event view w/ events " + dateEvts.size());
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.events);
        rv.setTextViewText(R.id.day, String.format(Locale.US,"%02d", date.monthDay));
        LOG.trace( "Weekday: " + date.weekDay);
        rv.setTextViewText(R.id.day_of_week, String.format(Locale.US,"[%s]", DateUtils
                .getDayOfWeekString(date.weekDay + 1, DateUtils.LENGTH_SHORT)));

        int dayBgColor = android.R.color.transparent;
        switch (date.weekDay+1) {
        case Calendar.SATURDAY:
            setColors(rv, R.color.color_saturday_text, R.color.color_saturday_background);
            break;
        case Calendar.SUNDAY:
            setColors(rv, R.color.color_sunday_text, R.color.color_sunday_background);
            break;
        default:
            setColors(rv, android.R.color.black, android.R.color.transparent);
            break;
        }

        rv.removeAllViews(R.id.events);
        if (DateUtils.isToday(date.toMillis(false))) {
            if (dateEvts.isEmpty()) {
                dateEvts.add(Event.EMPTY);
            }
            rv.setInt(R.id.day, "setBackgroundColor", mContext.getResources()
                    .getColor(R.color.color_today_background));
        }else{
            rv.setInt(R.id.day,"setBackgroundColor", mContext.getResources().getColor(dayBgColor));
        }

        if (!dateEvts.isEmpty()) {
            for (Event event : dateEvts) {
                    RemoteViews eventEntry = new RemoteViews(
                            mContext.getPackageName(), R.layout.event_entry);
                    StringBuilder textEntry = getEvtTimes(event);
                    textEntry.append(event.title);
                    eventEntry.setTextViewText(R.id.event_text,
                            textEntry.toString());
                    eventEntry.setTextColor(R.id.event_text, event.color);
                    rv.addView(R.id.events, eventEntry);
            }
        } else {
            rv.setViewVisibility(R.id.day_row, View.GONE);
        }

        // LOG.debug("RV created: "+rv);
        Intent fillInIntent = new Intent();
        rv.setOnClickFillInIntent(R.id.events, fillInIntent);
        return rv;
    }

    private void setColors(RemoteViews rv, int colorIdText,
            int colorIdBackground) {
        int textColor = mContext.getResources().getColor(colorIdText);
        int bgColor = mContext.getResources().getColor(colorIdBackground);
        rv.setTextColor(R.id.day_of_week, textColor);
        rv.setTextColor(R.id.day,textColor);
        rv.setInt(R.id.day_row, "setBackgroundColor", bgColor);
    }

    /**
     * if the event is starting today, the starting time will be returned
     * otherwise nothing
     *
     * @param event calendar event
     * @return the populated builder
     */
    private StringBuilder getEvtTimes(Event event) {
        StringBuilder result = new StringBuilder();
        if (!event.allDay) {
            if (EagleEyeAgendaWidgetConfigure.isShowStart(mContext, mWidgetId)) {
                result.append(DateUtils.formatDateTime(mContext, event.start,
                        DateUtils.FORMAT_SHOW_TIME
                                | DateUtils.FORMAT_ABBREV_TIME));
            }
            result.append("-");
            if (EagleEyeAgendaWidgetConfigure.isShowEnd(mContext, mWidgetId)) {
                result.append(DateUtils.formatDateTime(mContext, event.end,
                        DateUtils.FORMAT_SHOW_TIME
                                | DateUtils.FORMAT_ABBREV_TIME));
            }
            result.append(" ");
        }
        return result;
    }

    private RemoteViews getMonthView(int month, int year) {
        // LOG.debug("Returning month view for "+month);
        // final int itemId = (position % 2 == 0 ? R.layout.light_widget_item
        // : R.layout.dark_widget_item);
        RemoteViews rv = new RemoteViews(mContext.getPackageName(),
                R.layout.month_title);
        rv.setTextViewText(R.id.month_name,
                DateUtils.getMonthString(month, DateUtils.LENGTH_LONG));
        rv.setTextViewText(R.id.year_name,Integer.toString(year));
        return rv;
    }

    private Time getNow() {
        Time result = new Time();
        result.setToNow();
        return result;
    }

    private Time getEndOfDay(Time timeInDay) {
        Time result = new Time(timeInDay);
//		result.monthDay++;
//		result.set(timeInDay.toMillis(false) + DateUtils.DAY_IN_MILLIS);
        result.hour = 23;
        result.minute = 59;
        result.second = 59;
        result.normalize(false);
        return result;
    }


    private void updateRvs() {
        rvs = new RemoteViews[COUNT_VIEWS];
        int currentMonth = -1;
        int countMonthRows = 0;
        int countSkippedDays = 0;
        for (int position = 0; position < rvs.length; position++) {
            RemoteViews result;
            Time now = getNow();
            // the first one is always the month section
            if (position == 0) {
                // LOG.debug("position 0 - returning month view");
                // we need to add the month (current) here
                currentMonth = now.month;
                result = getMonthView(currentMonth, now.year);
                countMonthRows++;
            } else {
                // LOG.debug( "position != 0 - mCountMonthRows: " +
                // countMonthRows
                // + ", currentMont: " + currentMonth);
                int dayOffsetFromNow = position - countMonthRows
                        + countSkippedDays;
                Time positionTime = getPositionTime(dayOffsetFromNow);
                List<Event> daysEvts = listEventsForDay(positionTime);
                while (daysEvts.isEmpty()
                        && !DateUtils.isToday(positionTime.toMillis(false))
                        && countSkippedDays < MAX_SKIPPED_DAYS) {
                    LOG.trace(
                            "Detected empty day which is not today :) - skipping");
                    countSkippedDays++;
                    dayOffsetFromNow = position - countMonthRows
                            + countSkippedDays;
                    positionTime = getPositionTime(dayOffsetFromNow);
                    daysEvts = listEventsForDay(positionTime);
                }
                if (currentMonth != positionTime.month) {
                    // LOG.trace("month change detected ("+currentMonth+" -> "+positionTime.month+")");
                    result = getMonthView(positionTime.month, positionTime.year);
                    countMonthRows++;
                    currentMonth = positionTime.month;
                } else {
                    result = getDayEventView(positionTime, daysEvts);
                }
            }
            rvs[position] = result;
        }
        LOG.debug("Remote views are updated");
    }

    private Time getPositionTime(int dayOffsetFromNow) {
        Time positionTime = getNow();
        positionTime.monthDay = positionTime.monthDay+dayOffsetFromNow;
        positionTime.normalize(false);
        return positionTime;
    }

    @Override
    public void onDestroy() {
        LOG.debug( "onDestroy");
    }

    private List<Event> listEventsForDay(Time day) {
        List<Event> result = new ArrayList<CalendarRemoteViewsFactory.Event>();
        if (!DateUtils.isToday(day.normalize(false))) {
            day.hour = 0;
            day.minute = 0;
            day.second = 1;
        }
        Time endOfDay = getEndOfDay(day);
        LOG.trace( "listing events for: " + day + "midnight: " + endOfDay);
        Cursor c = CalendarContract.Instances.query(
                mContext.getContentResolver(), COLS, day.toMillis(false),
                endOfDay.toMillis(false));
        result.addAll(Event.listEvents(c, day));
        c.close();
        return result;

    }

    // private List<Event> listEventsForDay_thehardway(Time day) {
    // List<Event> result = new ArrayList<CalendarRemoteViewsFactory.Event>();
    // // the dashed values are shifted by the number of days given in the
    // // offset
    // if (!DateUtils.isToday(day.normalize(false))) {
    // day.hour = 0;
    // day.minute = 0;
    // day.second = 0;
    // }
    // Time endOfDay = getEndOfDay(day);
    // LOG.trace( "listing events for: " + day + "midnight: " + endOfDay);
    // long nowDash = day.toMillis(false);
    // long endOfDayDash = endOfDay.toMillis(false);
    //
    // String selection = getActiveCalendarsINSelection()
    // + CalendarContract.Events.DTSTART + " <= ? AND "
    // + CalendarContract.Events.DTEND + " >= ?";
    // String[] selArgs = new String[] { Long.toString(nowDash),
    // Long.toString(nowDash) };
    // String order = CalendarContract.Events.DTSTART + " ASC";
    // Cursor c = mContext.getContentResolver().query(
    // CalendarContract.Events.CONTENT_URI, COLS, selection, selArgs,
    // order);
    // result.addAll(Event.listEvents(c));
    // c.close();
    // selection = getActiveCalendarsINSelection()
    // + CalendarContract.Events.DTSTART + " >= ? AND "
    // + CalendarContract.Events.DTSTART + " <= ? AND "
    // + CalendarContract.Events.ALL_DAY + " == 0";
    // selArgs = new String[] { Long.toString(nowDash),
    // Long.toString(endOfDayDash) };
    // order = CalendarContract.Events.DTSTART + " ASC";
    // c = mContext.getContentResolver().query(
    // CalendarContract.Events.CONTENT_URI, COLS, selection, selArgs,
    // order);
    // result.addAll(Event.listEvents(c));
    // c.close();
    // return result;
    // }

    // private String getActiveCalendarsINSelection() {
    // StringBuilder sb = new StringBuilder(
    // CalendarContract.Events.CALENDAR_ID);
    // sb.append(" IN (");
    // for (String id : mActiveCalendarIds) {
    // sb.append(id);
    // sb.append(",");
    // }
    // sb.replace(sb.length() - 1, sb.length(), ") AND ");
    // return sb.toString();
    // }

    private static class Event {

        final String title;
        final long start;
        final long end;
        final boolean allDay;
        final int color;

        public static final Event EMPTY = new Event(" ", 0, 0, true,
                Color.TRANSPARENT);

        private Event(String title, long start, long end, boolean allDay,
                int color) {
            this.title = title;
            this.start = start;
            this.end = end;
            this.allDay = allDay;
            this.color = color;
        }

        /**
         *
         * @param c the cursor containing the events
         * @param forDay if this is set, a filter for all day events only showing on this day is applied
         * @return a list of created events
         */
        public static List<Event> listEvents(Cursor c, Time forDay) {
            // LOG.debug("Got cursor w/ "+c.getCount()+" entries");
            List<Event> result = new ArrayList<CalendarRemoteViewsFactory.Event>();
            while (c.moveToNext()) {
                String titel = c.getString(c
                        .getColumnIndex(CalendarContract.Events.TITLE));
                long start = c.getLong(c
                        .getColumnIndex(CalendarContract.Instances.BEGIN));
                long end = c.getLong(c
                        .getColumnIndex(CalendarContract.Instances.END));
                boolean allDay = c.getInt(c
                        .getColumnIndex(CalendarContract.Events.ALL_DAY)) == 1;
                int intColor = c
                        .getInt(c
                                .getColumnIndex(CalendarContract.Events.CALENDAR_COLOR));
                LOG.trace( String.format(
                        "New event: %1s [%2s - %3s] allDay: %4s, color: %5s",
                        titel, start, end, allDay, intColor));
                boolean ignoreEvent = false;
                if (forDay != null){
                    //this may be not working w/ negative DST and timezone settings ....
                    if (allDay){
                        Time startTime = new Time();
                        startTime.set(start);
                        if (forDay.monthDay != startTime.monthDay){
                            LOG.trace("ignoring duplicate allDay event entry on next day");
                            ignoreEvent = true;
                        }else{
                            ignoreEvent = false;
                        }
//						Time end = new Time();
//						end.set(event.end);
                        /*
                         * all day events will start and stop w/ DST offset - e.g.:
                         * All day event on the 24.09.2012 will have start: 20120924T020000
                         * end: 20120925T020000
                         */
//						LOG.debug( "All day event: "+date+" - start: "+start+" -end: "+end);
                    }
                }
                if (!ignoreEvent){
                    result.add(new Event(titel, start, end, allDay, getAlphaFixedColor(intColor)));
                }
            }
            return result;
        }

        /**
         * if the alpha channel is 0 , then invert the alpha channel
         * @param color color
         * @return int representation
         */
        private static int getAlphaFixedColor(int color) {
//			int a = Color.alpha(color);
//			int r = Color.red(color);
//			int g = Color.green(color);
//			int b = Color.blue(color);
//			LOG.trace( "ARGB: "+a+","+r+","+g+","+b);
            return Color.argb((Color.alpha(color)==0?255:Color.alpha(color)), Color.red(color), Color.green(color), Color.blue(color));
        }


    }

}
