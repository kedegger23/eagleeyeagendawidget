package com.edegger.calendar.widget;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import com.edegger.calendar.CalendarChangeReceiver;
import com.edegger.calendar.R;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobScheduler;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EagleEyeAgendaWidgetConfigure extends Activity {

    private static final Logger LOG = LoggerFactory.getLogger(EagleEyeAgendaWidgetConfigure.class);

    private static final String PREFS_NAME = EagleEyeAgendaWidgetProvider.class.getCanonicalName();
    private static final String PREF_PREFIX_KEY = "cal_widget_";
    private static final String PREF_CALS = "cal_ids_";
    private static final String PREF_SHOW_START = "show_start_";
    private static final String PREF_SHOW_END = "show_end_";
    private static final String PREF_LAUNCH_INTERNAL_CAL = "launch_internal_cal_";

    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    CheckBox mShowStart;
    CheckBox mShowEnd;
    CheckBox mLaunchInternalCal;
    Set<String> mSelectedCals = new HashSet<String>();

    @Override
    public void onCreate(Bundle icicle) {
        LOG.info("Configure.configure widget");
        super.onCreate(icicle);
        setResult(RESULT_CANCELED);

        // Set the view layout resource to use.
        setContentView(R.layout.calwidget_configure);
        mShowStart = (CheckBox) findViewById(R.id.prf_show_start);
        mShowEnd = (CheckBox) findViewById(R.id.prf_show_end);
        mLaunchInternalCal = (CheckBox) findViewById(R.id.prf_launch_internal_calendar);
        // Bind the action for the save button.
        findViewById(R.id.configure_ok).setOnClickListener(mOnClickListener);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If they gave us an intent without the widget id, just bail.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = EagleEyeAgendaWidgetConfigure.this;

            // When the button is clicked, save the string in our prefs and
            // return that they
            // clicked OK.
            LOG.debug( "selected Cals: " + mSelectedCals);
            savePref(context, mAppWidgetId);

            // // Push widget update to surface with newly set prefix
            // AppWidgetManager appWidgetManager = AppWidgetManager
            // .getInstance(context);
            // appWidgetManager.up
            // ReducedCalendarWidgetProvider.updateAppWidget(context,
            // appWidgetManager,
            // mAppWidgetId, titlePrefix);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            resultValue.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            setResult(RESULT_OK, resultValue);
//			AppWidgetManager.getInstance(context).
//			sendActionUpdateWidgetBroadcast(context);
//			context.sendBroadcast(resultValue);
            //ensures that the correct launch intents will be generated
            //new EagleEyeAgendaWidgetProvider().onUpdate(context, AppWidgetManager.getInstance(context), new int[]{mAppWidgetId});
            scheduleDailyUpdates(context);
            registerContentChangedJob(context);
            finish();
        }

    };

    private void registerContentChangedJob(Context ctx) {
        LOG.debug("registerContentChangedJob");
        UpdateCalendarWidgetJob.scheduleContentObserverJob(ctx);
    }

    /**
     * ensures the widget will be updated every midnight even if no calender changes have been
     * detected...
     */
    public static void scheduleDailyUpdates(Context ctx) {
        LOG.debug("Configure.scheduling regular updates");
        //update widget using the calendar change broadcast receiver
        Intent intent = new Intent(ctx, CalendarChangeReceiver.class);
        //intent.setAction("android.intent.action.PROVIDER_CHANGED");

        PendingIntent alarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        // Set the alarm to start at approximately 0:10 a.m.
        /*
                */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        /* FIX ME: remove debugging setting / alarm interval
        */
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE,10);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH,1);
        alarmMgr.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_HALF_DAY, alarmIntent);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.add(Calendar.MINUTE,2);
//        alarmMgr.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
//                AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);
        LOG.debug("scheduleDailyUpdates: TWICE A DAY starting from "+SimpleDateFormat
                .getDateTimeInstance(SimpleDateFormat.MEDIUM,SimpleDateFormat.LONG)
                .format(calendar.getTime()));
        /*
        // Set the alarm to start now+1h and every 60 min
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis()+DateUtils.HOUR_IN_MILLIS);
        alarmMgr.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_HOUR, alarmIntent);
        LOG.debug("scheduleDailyUpdates: 60MIN starting from "+SimpleDateFormat
                .getDateTimeInstance(SimpleDateFormat.MEDIUM,SimpleDateFormat.LONG)
                .format(calendar.getTime()));
                */
    }

    private void savePref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(
                PREFS_NAME, 0).edit();
        prefs.putStringSet(PREF_PREFIX_KEY + PREF_CALS + appWidgetId,
                mSelectedCals);
        prefs.putBoolean(PREF_PREFIX_KEY + PREF_SHOW_START + appWidgetId,
                mShowStart.isChecked());
        prefs.putBoolean(PREF_PREFIX_KEY + PREF_SHOW_END + appWidgetId,
                mShowEnd.isChecked());
        prefs.putBoolean(PREF_PREFIX_KEY + PREF_LAUNCH_INTERNAL_CAL+ appWidgetId,
                mLaunchInternalCal.isChecked());
        prefs.apply();

    }


    public static boolean isShowStart(Context context, int widgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getBoolean(PREF_PREFIX_KEY +PREF_SHOW_START+ widgetId,
                false);
    }

    public static boolean isShowEnd(Context context, int widgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getBoolean(PREF_PREFIX_KEY +PREF_SHOW_END+ widgetId,
                false);
    }

    public static boolean launchGoogleCalendar(Context context, int widgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getBoolean(PREF_PREFIX_KEY +PREF_LAUNCH_INTERNAL_CAL+ widgetId,
                false);
    }


}
