package com.edegger.calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.edegger.calendar.widget.EagleEyeAgendaWidgetConfigure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ( ͡°͜ ͡°)
 * Created by EdeggerK on 26.05.2017.   ¯\_(ツ)_/¯
 */

public class BootCompleteReceiver extends BroadcastReceiver {

    private static final Logger LOG = LoggerFactory.getLogger(BootCompleteReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        LOG.debug( "BootCompleteReceiver - device's up and running! "+intent.toUri(Intent.URI_INTENT_SCHEME));
        EagleEyeAgendaWidgetConfigure.scheduleDailyUpdates(context);
    }

}
