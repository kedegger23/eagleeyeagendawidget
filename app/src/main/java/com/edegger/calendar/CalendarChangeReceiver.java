package com.edegger.calendar;

import com.edegger.calendar.widget.EagleEyeAgendaWidgetProvider;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalendarChangeReceiver extends BroadcastReceiver{

    private static final Logger LOG = LoggerFactory.getLogger(CalendarChangeReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        LOG.debug( "Calender change detected: "+intent.toUri(Intent.URI_INTENT_SCHEME));
        notifyWidgets(context);
    }

    private void notifyWidgets(Context ctx) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(ctx);
        ComponentName widgetComponent = new ComponentName(ctx,EagleEyeAgendaWidgetProvider.class);
        int[] widgetIds = appWidgetManager.getAppWidgetIds(widgetComponent);
        appWidgetManager.notifyAppWidgetViewDataChanged(widgetIds, R.id.evt_list);
    }

}


